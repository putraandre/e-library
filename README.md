How to install e-library using git

# install Vue cli
npm install -g @vue/cli

# clone repository
git clone git@gitlab.com:putraandre/e-library.git

# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
